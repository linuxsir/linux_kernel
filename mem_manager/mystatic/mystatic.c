/*
 * =====================================================================================
 *
 *       Filename:  mystatic.c
 *
 *    Description:  测试Static修饰的变量
 *
 *        Version:  1.0
 *        Created:  2015年09月15日 16时35分34秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */

#include <stdio.h>

int  add()
{
    static int count = 11;//此初始化只执行一次哦
    return count--;
}
int count = 0;

int main(void)
{
    printf("global\t\tlocal static\n");
    for (; count <10; ++count) {
       printf("%d\t\t%d\n",count,add() ); 
    }
    return 0;
}
