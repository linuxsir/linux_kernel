/*
 * =====================================================================================
 *
 *       Filename:  memdev.h
 *
 *    Description:  内存虚拟的字符设备的驱动程序
 *
 *        Version:  1.0
 *        Created:  2015年09月17日 19时51分20秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */

#include <linux/semaphore.h>
#include <linux/wait.h>
#ifndef _MEMDEV_H_
#define _MEMDEV_H_ value

#ifndef MEMDEV_MAJOR
#define MEMDEV_MAJOR 271  /*预设的主设备号*/
#endif
#ifndef MEMDEV_NR_DEVS
#define MEMDEV_NR_DEVS 2  /*设备数*/
#endif

#ifndef MEMDEV_SIZE
#define MEMDEV_SIZE 4096
#endif

/**
 * @brief 模拟设备的对象
 */
struct mem_dev{
    bool canRead;
    bool canWrite;
    char *data;
    unsigned long size;
    unsigned long wpos;
    unsigned long rpos;
    unsigned long nattch;
    wait_queue_head_t rwq;
    wait_queue_head_t wwq;
    
};

#define MEMDEV_IOC_MAGIC    'k'

#define MEMDEV_IOCPRINT _IO(MEMDEV_IOC_MAGIC,1)
#define MEMDEV_IOCGETDATA   _IOR(MEMDEV_IOC_MAGIC,2,int)
#define MEMDEV_IOCSETDATA   _IOW(MEMDEV_IOC_MAGIC,3,int)

#define MEMDEV_IOC_MAXNR    3



#endif
