/*
 * =====================================================================================
 *
 *       Filename:  mytimer.c
 *
 *    Description:  内核定时器实验
 *
 *        Version:  1.0
 *        Created:  2015年09月15日 13时21分23秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */
#include <linux/module.h>
#include <linux/init.h>
#include <linux/moduleparam.h>
#include <linux/timer.h>
#include <asm/uaccess.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("guicai.ma@qq.com");
MODULE_DESCRIPTION("the timer_list");
static int delay;

module_param(delay,int,0644);
struct timer_list timer;

void timer_function(unsigned long para){
    static int count = 1;
    if(count < para){
        if(1==count)
            printk("<1>\nTimer counter begins: ");
        printk("<1>count = %d ",count);
        mod_timer(&timer,jiffies+(HZ/2));
    }else
    {
        printk("<1>###");
        printk("<1>Timer Expired %ld(Specified by para) times,\n"
                "and you should remove the module to destroy it\n",para);
    }
    count ++;
}
static int __init mytimer_init(void)
{

    init_timer(&timer);
    
    return 0;
}
static void __exit mytimer_exit(void)
{

}
module_init(mytimer_init);
module_exit(mytimer_exit);


