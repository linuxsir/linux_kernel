/*
 * =====================================================================================
 *
 *       Filename:  app2.c
 *
 *    Description:  测试访问memdev设备文件
 *
 *        Version:  1.0
 *        Created:  2015年09月18日 01时31分04秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[])
{
    FILE *filp = NULL;
    char Buf[4096];
    strcpy(Buf,"Mem is cdev devices\n");
    printf("Buf:%s\n", Buf);

    filp = fopen("/dev/memdev0","r+");
    if(filp == NULL)
    {
        printf("Open memdev error!\n");
        perror("fopen");
        return -1;
    }
    fwrite(Buf , sizeof(Buf) ,1 , filp);
    fseek(filp,0,SEEK_SET);

    strcpy(Buf,"Buf is new!\n");
    printf("origin Buf:%s\n",Buf);

    fread(Buf , sizeof(Buf) ,1 , filp);
    printf("read Buf:%s\n",Buf);
    
    return 0;
}
