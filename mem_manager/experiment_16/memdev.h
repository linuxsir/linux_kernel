/*
 * =====================================================================================
 *
 *       Filename:  memdev.h
 *
 *    Description:  内存虚拟的字符设备的驱动程序
 *
 *        Version:  1.0
 *        Created:  2015年09月17日 19时51分20秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */

#ifndef _MEMDEV_H_
#define _MEMDEV_H_ value

#ifndef MEMDEV_MAJOR
#define MEMDEV_MAJOR 251  /*预设的主设备号*/
#endif
#ifndef MEMDEV_NR_DEVS
#define MEMDEV_NR_DEVS 2  /*设备数*/
#endif

#ifndef MEMDEV_SIZE
#define MEMDEV_SIZE 4096
#endif

struct mem_dev{
    char *data;
    unsigned long size;
};




#endif
