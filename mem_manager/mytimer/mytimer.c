/*
 * mytimer.c
 *
 *  Created on: 2015年9月15日
 *      Author: linux
 */

#include <linux/module.h>
#include <linux/timer.h>
#include <linux/moduleparam.h>
#include <linux/kernel.h>
#include <asm/uaccess.h>



struct timer_list timer;
static int times = 1;
module_param(times,int,S_IRUGO);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("linux");
MODULE_DESCRIPTION("......");

void timer_function(int para)
{

    
	static int count = 1;
    printk("<1> times = %d\n",times);
    printk("<1>magc: count = %d\n",count);
	if(count <= para)
	{
		if(1==count)
			printk("<1>\n Timer counter begins:");
		printk("<1>count = %d",count);
		mod_timer(&timer,jiffies + (HZ/2));

	}else
	{
		printk("<1>###");
		printk("<1>Timer expired %ld(specified by para) Times,\n"
				"and you should remove the module to destroy it\n");
	}
	count++;
}
int __init mytimer_init(void)
{
	init_timer(&timer);
	timer.data = times;
	timer.expires = jiffies + (HZ/2);
	timer.function = timer_function;
	add_timer(&timer);




	return 0;
}


static void __exit mytimer_exit(void)
{
	del_timer(&timer);
	printk("<1>Timer has been destroyed \n");


}

module_init(mytimer_init);
module_exit(mytimer_exit);





