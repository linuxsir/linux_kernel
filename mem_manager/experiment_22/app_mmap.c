/*
 * =====================================================================================
 *
 *       Filename:  mmap.c
 *
 *    Description:  应用mmap系统调用接口示例
 *
 *        Version:  1.0
 *        Created:  2015年09月21日 11时32分54秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */


#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <string.h>
#include <fcntl.h>

int main(int argc, char *argv[])
{
    int fd;
    char buf[100];
    char *start;
    //1.生成fd
    if(-1 == (fd=open("/dev/memdev0",O_RDWR))){
        perror("open testfile error!\n");
        _exit(EXIT_FAILURE);
    }
    //2.创建内存映射区
    start =(char *) mmap(NULL,100,PROT_READ|PROT_WRITE,MAP_SHARED,fd,0);
    //写入数据
    strcpy(start,"Buf is not NULL!\n");

    //读出数据
    strcpy(buf,start);
    printf("buf = %s\n",buf);


    //解除映射
    munmap(start,100);
    close(fd);
    return 0;
}
