/*
 * =====================================================================================
 *
 *       Filename:  mem_main.c
 *
 *    Description:  内存分配练习
 *
 *        Version:  1.0
 *        Created:  2015年09月11日 16时43分20秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/slab.h>
#include <linux/vmalloc.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("guicai.ma@qq.com");
MODULE_DESCRIPTION("内存分配实验");
MODULE_ALIAS("malloc module");

unsigned char *pagemem;
unsigned char *pagezmem;
unsigned char *pagesmem;
unsigned char *kmallocmem;
unsigned char *vmallocmem;

 int __init mem_alloc_init(void)
{
    
    pagemem = (unsigned char *)__get_free_page(GFP_KERNEL);
    printk("<1>get_free_page:pagemem va addr=%p" "\tpa addr=%lx\n", pagemem,__pa(pagemem));
    pagesmem = (unsigned char *)__get_free_pages(GFP_KERNEL,3);
    printk("<1>get_free_pages:pagesmem va addr=%p" "\tpa addr=%lx\n", pagesmem,__pa(pagesmem));
    pagezmem = (unsigned char *)get_zeroed_page(GFP_KERNEL);
    printk("<1>__get_zeroed_page:pagezmem va addr=%p" "\tpa addr=%lx\n", pagezmem,__pa(pagezmem));
    kmallocmem = (unsigned char *)kmalloc(100,GFP_KERNEL);
    memset(kmallocmem,0,100);
    strcpy(kmallocmem,"<<< --- Kmalloc OK! ---->>>>");
    printk("<1>kmalloc:kmalloc  va addr=%p" "\tpa addr=%lx\n", kmallocmem,__pa(kmallocmem));
    printk("<1>kmalloc :kmallocmem say  %s\n",kmallocmem );

    vmallocmem = (unsigned char *)vmalloc(1000000);
    printk("<1>vmalloc:vmallocmem va addr=%p\n ",vmallocmem);


    return 0;
}
static void __exit mem_alloc_exit(void)
{

    free_page((int)pagemem);
    free_page((int)pagezmem);
    free_pages((int)pagesmem,2);
    kfree(kmallocmem);
    vfree(vmallocmem);
    printk("<1><< -- Module Exit ! ---->>> ");
}








module_init(mem_alloc_init);
module_exit(mem_alloc_exit);

