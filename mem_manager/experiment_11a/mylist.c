/*
 * =====================================================================================
 *
 *       Filename:  mylist.c
 *
 *    Description:  内核链表练习
 *
 *        Version:  1.0
 *        Created:  2015年09月14日 16时21分34秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */
#include <linux/module.h>
#include <linux/init.h>
#include <linux/slab.h>


struct student{
    char name[100];
    int num;
    struct list_head list;
};

struct student *pstudent;
struct student *temp_student;
struct list_head student_list;
struct list_head *pos;

MODULE_LICENSE("GPL");
MODULE_AUTHOR("guicai.ma@qq.com");
MODULE_DESCRIPTION("the list of kernel");

static int __init mylist_init(void)
{
    
    INIT_LIST_HEAD(&student_list);
    pstudent = (struct student *)kmalloc(sizeof(struct student)*5,GFP_KERNEL);
    memset(pstudent,0,sizeof(struct student)*5);
    int i;
    for (i = 0; i < 5; ++i) {
        sprintf(pstudent[i].name ,"Student%d",i+1);
        pstudent[i].num = i+1;
        list_add(&(pstudent[i].list),&student_list);
    }

    list_for_each(pos,&student_list)
    {
        temp_student = list_entry(pos,struct student,list);
        printk("<0>student %d name: %s\n",temp_student->num,temp_student->name );
    }


    return 0;
}
static void __exit mylist_exit(void)
{
    int i = 0;
    for(i=0;i<5;i++)
    {
        list_del(&(pstudent[i].list));

    }
    kfree(pstudent);



}
module_init(mylist_init);
module_exit(mylist_exit);






