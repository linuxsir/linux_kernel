/*
 * =====================================================================================
 *
 *       Filename:  klist.c
 *
 *    Description:  隔离的内核链表
 *
 *        Version:  1.0
 *        Created:  2015年09月14日 18时29分23秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */

#include <linux/types.h>
#include <linux/slab.h>

struct student{
    char name[100];
    int num;
    struct list_head list;
};

struct student *pstudent;
struct student *temp_student;
struct list_head student_list;
struct list_head *pos;

void list_init()
{
    int i = 0;
    INIT_LIST_HEAD(student_list);
    pstudent = (struct student *)kmalloc((sizeof(struct student)*5),GFP_KERNEL);
    memset(pstudent,0,sizeof(struct student));
    for (i = 0; i < 5; ++i) {
        sprintf(pstudent[i].name,"Student%d",i+1);
        pstudent[i].num = i+1;
        list_add(&(pstudent[i].list),&student_list);
    }

    list_for_each(pos,&student_list)
    {
        temp_student = list_entry(pos,struct student , list);
        printk("<1>student%dname: %s\n",temp_student->num,temp_student->name );
    }
    

}

void list_destroy()
{
    int i = 0;
   for (i = 0; i < 5; ++i) {
       list_del(&(pstudent[i].list));
   } 
   kfree(pstudent);
} 
