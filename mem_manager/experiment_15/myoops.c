/*
 * =====================================================================================
 *
 *       Filename:  myoops.c
 *
 *    Description:  OOPS异常分析练习
 *
 *        Version:  1.0
 *        Created:  2015年09月16日 16时17分16秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */

#include <linux/module.h>
#include <linux/init.h>
#include <linux/kernel.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("guicai.ma@qq.com");
MODULE_DESCRIPTION("analysis a oops");

int d()
{
    int *p = NULL;
    int a = 6;
    printk("<1>Function D\n");
    *p = a + 5;
}
int c()
{
    printk("Function C!\n");
    d();
}
int b()
{
    printk("Function B!\n");
    c();
}
int a()
{
    printk("Function A!\n");
    b();

} 
static int __init myoops_init(void)
{
    printk("start init\n");
    a();
    return 0;
}
static void __exit myoops_exit(void)
{

}
module_init(myoops_init);
module_exit(myoops_exit);
