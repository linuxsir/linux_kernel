/*
 * =====================================================================================
 *
 *       Filename:  other_dir.c
 *
 *    Description:  同模块下，不同目录下的源码文件other_dir.c
 *
 *        Version:  1.0
 *        Created:  2017年01月03日 21时11分03秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */

#include <linux/module.h>

void other_dir_function(void)
{
    printk("call %s \n",__FUNCTION__);
}
