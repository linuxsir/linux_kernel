# 目标：演示自旋锁的使用

#　要点
先构建一个多内核线程的环境
自旋锁：
１）初始化　
DEFINE_SPINLOCK()
spin_lock_init(&lock_name)

2)锁的使用
在被保护的操作前后添加持锁和解锁的操作
spin_lock(&lock_name)
spin_unlock(&lock_name)



