#include <linux/module.h>
#include "other.h"
#include "other_dir.h" // 此种写法需要在编译时指定　-I选项，要么就把相对路径写出来
MODULE_LICENSE("GPL");
MODULE_AUTHOR("MAGC");
MODULE_DESCRIPTION("Magc Module Compile");


static int minit(void)
{
    printk("call %s \n",__FUNCTION__);
    other_function();
    other_dir_function();
    return 0;
}

static void mexit(void)
{

    printk("call %s \n",__FUNCTION__);
}
module_init(minit);
module_exit(mexit);
