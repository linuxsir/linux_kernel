# 问题：在编译此模块时，因它引用了module2的一个函数，编译时会报warning:找不到符号add_mod , 
# 原因及对策
因为此时是单独编译此模块，并没有找到另一个模块module2的符号表，所以，可以将module2下的Module.symbol文件复制过来，编译时就不会报异常信息了

注：
1) 这里只是以Ｗａｒｎｉｎｇ方式提示，是考虑到单独编译模块时中以找不到，期望在安装时可以补充（先安装被引用的模块就可以）

2) 安装时先安装被依赖的模块，卸载时先卸载没有依赖的模块。


