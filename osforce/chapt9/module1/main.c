#include <linux/module.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("MAGC");
MODULE_DESCRIPTION("Magc Module Compile");

extern int add_mod(int a , int b);

static int minit(void)
{
    printk("call %s \n",__FUNCTION__);
    printk("add_mod(2 , 3) = %d\n",add_mod(2,3));
    return 0;
}

static void mexit(void)
{

    printk("call %s \n",__FUNCTION__);
}
module_init(minit);
module_exit(mexit);
