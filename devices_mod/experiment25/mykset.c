/*
 * =====================================================================================
 *
 *       Filename:  mykset.c
 *
 *    Description:  kset应用
 *    热插拔事件处理机制
 *
 *        Version:  1.0
 *        Created:  2015年09月22日 16时08分25秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/sysfs.h>
#include <linux/kobject.h>
#include <linux/stat.h>
#include <linux/string.h>
#include <linux/init.h>


MODULE_AUTHOR("guicai.ma@qq.com");
MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("kset for  ");

struct kset mykset1,mykset2;

static int my_filter(struct kset *kset , struct kobject *kobj){
    printk("from my_filter ..\n");
    return 1;
}

static const char *my_name(struct kset *kset , struct kobject *kobject){
    printk("from my_name \n");
    static char name[20];
    printk(" kobject name :%s\n",kobject->name);
    sprintf(name , "%s","kset_name");
    return name;
}
static int my_uevent(struct kset *kset , struct kobject *kobj ,struct kobj_uevent_env *env){
    
    int i = 0;
    printk("uevent object name:%s\n",kobj->name);
    while(i<env->envp_idx){
        printk("env : %s \n",env->envp[i]);
        i++;

    } 
    return 0; 
}
struct kset_uevent_ops uevent_ops = {
    .name = my_name,
    .filter = my_filter,
    .uevent = my_uevent 
};

static int __init kset_module_init(void)
{
    //初始化kset
    printk("kset module init ...\n");
    kobject_set_name(&mykset1.kobj,"kset1_kobj");
    printk("kset1 module init ...\n");
    mykset1.uevent_ops = &uevent_ops;
    printk("kset2 module init ...\n");
    mykset1.kobj.parent = NULL;
    kset_register(&mykset1);
    printk("kset3 module init ...\n");

    /*
    
    kobject_set_name(&mykset2.kobj,"kset2_kobj");
    mykset2.kobj.kset = &mykset1;
    kset_register(&mykset2);
    */
    return 0;
}
static void __exit kset_module_exit(void)
{

   kset_unregister(&mykset2);
   kset_unregister(&mykset1); 
}
module_init(kset_module_init);
module_exit(kset_module_exit);
