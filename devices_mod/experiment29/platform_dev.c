/*
 * =====================================================================================
 *
 *       Filename:  platform_dev.c
 *
 *    Description:  platform_device部分
 *
 *        Version:  1.0
 *        Created:  2015年09月23日 23时09分56秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */

#include <linux/module.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/string.h>
#include <linux/device.h>
#include <linux/platform_device.h>

MODULE_AUTHOR("guicai.ma@qq.com");
MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("a platform_device");

struct platform_device *mypdev;
static int __init platform_dev_init(void)
{
    //创建platform_device ,指定设备名（将来作为与驱动接头的暗号）
    mypdev = platform_device_alloc("mypdev",-1);

    //注册platform_device到内核中
    int ret = platform_device_add(mypdev);
    if(ret)
        platform_device_put(mypdev);

    return ret;
}
static void __exit platform_dev_exit(void)
{
    platform_device_unregister(mypdev);
}
module_init(platform_dev_init);
module_exit(platform_dev_exit);
