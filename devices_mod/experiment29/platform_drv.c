/*
 * =====================================================================================
 *
 *       Filename:  platform_drv.c
 *
 *    Description:  platform_driver模块
 *
 *        Version:  1.0
 *        Created:  2015年09月23日 23时12分17秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/string.h>
#include <linux/init.h>
#include <linux/platform_device.h>


MODULE_AUTHOR("guicai.ma@qq.com");
MODULE_DESCRIPTION("a platform_driver");
MODULE_LICENSE("GPL");

static int my_probe(struct platform_driver *drv){
    printk("I found the deivce for my driver!\n");
    return 0;
}
static int my_remove(struct platform_driver *drv){
    printk("remove the driver:%s\n",drv->driver.name);
    return 0;
}

/**
 * @brief 定义驱动实例
 * 通过它的driver成员来定义驱动基本信息（包括驱动名称）
 */
struct platform_driver mydrv = {
    .probe = my_probe,
    .remove = my_remove,
    .driver = {
        .owner = THIS_MODULE,
        .name = "mypdev",
    }
};

static int __init platform_drv_init(void)
{
    //注册驱动
    int ret = platform_driver_register(&mydrv);
    if(ret)
    {
        printk(KERN_DEBUG "platform_driver_register error\n" );
    } 
    return ret;
}
static void __exit platform_drv_exit(void)
{

    platform_driver_unregister(&mydrv);
}
module_init(platform_drv_init);
module_exit(platform_drv_exit);

