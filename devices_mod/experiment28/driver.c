/*
 * =====================================================================================
 *
 *       Filename:  driver.c
 *
 *    Description:  总线型设备驱动模型实验之驱动部分
 *
 *        Version:  1.0
 *        Created:  2015年09月23日 20时39分15秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */

#include <linux/module.h>
#include <linux/init.h>
#include <linux/device.h>
#include <linux/kernel.h>

MODULE_AUTHOR("guicai.ma@qq.com");
MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("a struct driver");

extern struct bus_type mybus;
static char *Version = "V1.0.2";

/**
 * @brief 定义probe函数，
 * 在总线匹配同名的device和driver后，调用此函数
 *
 * @param dev
 *
 * @return 
 */
static int driver_probe(struct device *dev){
    printk("found the driver and device !!!\n");
    return 0;
}
struct device_driver mydriver = {
    .name = "mydev",
    .bus = &mybus,
    .probe = driver_probe
};
static ssize_t show_version(struct device_driver *driver , char *buf){
    return sprintf(buf ,"%s\n",Version);
}

/**
 * @brief 定义一个drvier_attribue:driver_attr_version
 *
 * @param version
 * @param S_IRUGO
 * @param show_version
 * @param NULL
 */
DRIVER_ATTR(version,S_IRUGO,show_version,NULL);
static int __init driver_module_init(void)
{
    driver_register(&mydriver);
    driver_create_file(&mydriver , &driver_attr_version);
    return 0;
}
static void __exit driver_module_exit(void)
{
   driver_unregister(&mydriver); 
}
module_init(driver_module_init);
module_exit(driver_module_exit);

