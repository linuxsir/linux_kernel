/*
 * =====================================================================================
 *
 *       Filename:  mybus.c
 *
 *    Description:  bus_type 创建一个总线
 *
 *        Version:  1.0
 *        Created:  2015年09月22日 19时14分22秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */
#include <linux/module.h>
#include <linux/device.h>
#include <linux/init.h>
#include <linux/sysfs.h>
#include <linux/string.h>

MODULE_AUTHOR("guicai.ma@qq.com");
MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("a test for bus_type");

/**
 * @brief 定义总线的match方法
 *
 * @param dev
 * @param drv
 *
 * @return 
 */
static int mybus_match(struct device *dev , struct device_driver *drv){
    return !strncmp(dev->bus->name,drv->name , strlen(drv->name));
}

/**
 * @brief 定义总线
 */
struct bus_type bus = {
    .name = "magc_bus ",
    .match = mybus_match
};

/**
 * @brief 定义属性的show方法
 *
 * @param bus
 * @param buf
 *
 * @return 
 */
static ssize_t show_version(struct bus_type *bus , char *buf){

    return sprintf(buf , "%s","Version 1.0.1\n");

}

/**
 * @brief 初始化一个属性
 *
 * @param version
 * @param S_IRUGO
 * @param show_version
 * @param NULL
 */
static BUS_ATTR(version,S_IRUGO,show_version , NULL);

static int __init mybus_init(void)
{
    //注册一个总线
    int ret = bus_register(&bus);
    if(ret)
       return ret;
    //创建属性文件
    if(bus_create_file(&bus,&bus_attr_version ))
        printk(KERN_NOTICE"failed to create bus_attribute\n");
     
    return 0;
}
static void __exit mybus_exit(void)
{
    bus_unregister(&bus);

}
module_init(mybus_init);
module_exit(mybus_exit);

