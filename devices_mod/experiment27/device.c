/*
 * =====================================================================================
 *
 *       Filename:  device.c
 *
 *    Description:  总线和设备实验之device模块
 *
 *        Version:  1.0
 *        Created:  2015年09月23日 14时41分24秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */


#include <linux/module.h>
#include <linux/init.h>
#include <linux/string.h>
#include <linux/device.h>

MODULE_AUTHOR("guicai.ma@qq.com");
MODULE_DESCRIPTION("a device");
MODULE_LICENSE("GPL");

extern struct bus_type mybus;
extern struct device my_bus_dev;
static ssize_t show_device_attr(struct device *dev ,struct device_attribute *attr , char *buf){
    return sprintf(buf ,"%s\n","this is my device attrubute");
}
DEVICE_ATTR(version ,S_IRUGO , show_device_attr,NULL);
static void dev_release(struct device *dev){
    printk(KERN_DEBUG "%s release\n", dev_name(dev));
}
struct device mydev = {
    .bus = &mybus,
    //.parent = &my_bus_dev,
    .release = dev_release
};
static int __init device_module_init(void)
{
    dev_set_name(&mydev , "mydev");
    device_register(&mydev);
    device_create_file(&mydev , &dev_attr_version);
    return 0;
}
static void __exit device_module_exit(void)
{
    device_unregister(&mydev);

}
module_init(device_module_init);
module_exit(device_module_exit);
