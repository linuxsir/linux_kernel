/*
 * =====================================================================================
 *
 *       Filename:  mykobject.c
 *
 *    Description:  kobject对象
 *
 *        Version:  1.0
 *        Created:  2015年09月22日 10时19分39秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/sysfs.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("guicai.ma@qq.com");
MODULE_DESCRIPTION("a kobject ");

ssize_t attr_show(struct kobject *kobj,struct attribute *attrs,char *buf);
ssize_t attr_store(struct kobject *kobj ,struct attribute *attrs, const char *buf ,size_t len);

    
struct kobject kobj;
    //准备属性
    
struct sysfs_ops test_ops = {
    .store = attr_store,
    .show = attr_show
};
    
struct attribute test_attr = {
        .name= "kobj_config",
        .mode = S_IRWXUGO
    };
struct attribute *def_attrs[] = {
    &test_attr,
    NULL
};

struct kobj_type obtype = {
    .sysfs_ops = &test_ops,
    .default_attrs = def_attrs
};

ssize_t attr_show(struct kobject *kobj,struct attribute *attrs,char *buf){
    printk("have show!!\n");
    printk("attribute is %s \n",attrs->name);
    sprintf(buf,"%s\n",attrs->name);
    return strlen(attrs->name)+2; 
}
ssize_t attr_store(struct kobject *kobj ,struct attribute *attrs, const char *buf ,size_t len){
    printk("have store !\n");
    printk("write buf is %s\n",buf);
    return len; 
}
static int __init kobject_module_init(void)
{

    printk("kobject test init!\n");
    kobject_init_and_add(&kobj,&obtype,NULL,"kobj_test");
    return 0;
}
static void __exit kobject_module_exit(void)
{
    kobject_del(&kobj);

}
module_init(kobject_module_init);
module_exit(kobject_module_exit);
