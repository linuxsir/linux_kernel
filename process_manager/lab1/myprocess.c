/*
 * =====================================================================================
 *
 *       Filename:  myprocess.c
 *
 *    Description:  fork()
 *
 *        Version:  1.0
 *        Created:  2016年12月23日 16时08分31秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */

#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>


int main(void)
{
    pid_t pid;
    int static counter = 5;
    printf("init static counter= %d\n",counter );
    printf(" main process pid = %d\n",getpid());
    pid = fork();//一次调用，两次返回
    if(pid < 0)
    {
        fprintf(stderr, "fork failed ....\n");
    }
    else if(pid == 0)
    {
        counter ++;
        printf("I am child processs pid = %d counter = %d pointer = %p\n",getpid(),counter,&counter );
        counter ++;
        printf("after child process ++ ,counter = %d pointer = %p\n",counter,&counter);
    }
    else
    {
        counter ++;
        printf("I am parentt process pid = %d count = %d pointer = %p\n",getpid(),counter,&counter );
    }
    /**
     * 上面打印counter地址，输出的是该变量的虚拟地址，子进程从父进程复制而来的，虚拟地址空间是相同的，因此，此处两个进程打印地址也是相同的。
     * /
    return 0;
}
