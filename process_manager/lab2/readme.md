# 演示目的
fork()调用

循环两次，但共有三个进程，体会父子进程共享代码段的效果，子进程是完全从fork()语句开始，与父进程并行执行的。

![](./lab2.png)
[详细参考](http://www.linuxidc.com/Linux/2015-03/114888.htm) 


星期二, 27. 十二月 2016 02:51下午 

