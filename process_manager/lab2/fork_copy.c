/*
 * =====================================================================================
 *
 *       Filename:  fork_copy.c
 *
 *    Description:  体会fork()过程中的内存复制机制，子进程复制父进程的数据空间，堆和栈，但注意父子进程共享代码段，
 *
 *        Version:  1.0
 *        Created:  2016年12月26日 23时36分16秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */

#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <errno.h>
#define BUFSIZE 512
#define LEN 2

void err_exit(char *fmt,...);

int main(int argc, char *argv[])
{
    pid_t pid;
    int loop;
    printf("This is main process ,PID=%d\n",getpid() );
    for (loop = 0; loop < LEN; loop++) {
        printf("Now is No.%d loop:\n",loop );
        if((pid = fork())<0)
            fprintf(stderr, "%s\n","error in fork :" );
        else if(pid == 0)
        {
            printf("[Child process ]PID=%d PPID:%d\n",getpid(),getppid());
        }
        else
        {
            sleep(5);
        }
        
    }
    return 0;
}
