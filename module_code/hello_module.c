/*
 * =====================================================================================
 *
 *       Filename:  hello_module.c
 *
 *    Description:  模块练习
 *
 *        Version:  1.0
 *        Created:  2015年09月08日 16时23分08秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */
#include <linux/module.h>
#include <linux/init.h>


MODULE_LICENSE("GPL");
MODULE_AUTHOR("magc");
MODULE_DESCRIPTION("Hello Module");

static int __init hello_init(void)
{
    printk(KERN_ERR "hello world!\n");
    return 0;
}

static void __exit hello_exit(void)
{
    printk(KERN_ERR "hello_exit!\n");
}

module_init(hello_init);
module_exit(hello_exit);


