/*
 * =====================================================================================
 *
 *       Filename:  call_symbol.c
 *
 *    Description:  调用从a其它内核模块中导出的符号
 *
 *        Version:  1.0
 *        Created:  2015年09月11日 14时21分28秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
MODULE_LICENSE("GPL");
MODULE_AUTHOR("guicai.ma@qq.com");
MODULE_DESCRIPTION("从内核中导出符号表");

extern int add_integar(int a , int b);
extern int sub_integar(int a , int b);

static int __init export_symbol_init(void)
{
    int res = add_integar(3,4);
    printk(KERN_ALERT "hello,init,res=%d\n",res );
    return 0;
}
static void __exit export_symbol_exit(void)
{
    int res = sub_integar(6,2);
    printk(KERN_ALERT "hello exit ,res=%d\n",res );

}
module_init(export_symbol_init);
module_exit(export_symbol_exit);

