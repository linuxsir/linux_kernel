/*
 * =====================================================================================
 *
 *       Filename:  calculate.c
 *
 *    Description:  
 *    负责简单运算的函数集
 *
 *        Version:  1.0
 *        Created:  2015年09月11日 14时30分23秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */
#include <linux/init.h>
#include <linux/module.h>
MODULE_LICENSE("GPL");

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  add_integar
 *  Description:  
 * =====================================================================================
 */
    int        
add_integar ( int a , int b )
{
    return a+b; 
}		/* -----  end of function add_integar  ----- */

int sub_integar(int a , int b)
{
    return a-b;
}
static int __init calculate_init(void)
{
    return 0;
}
static void __exit calculate_exit(void)
{

}
module_init(calculate_init);
module_exit(calculate_exit);
EXPORT_SYMBOL(add_integar);
EXPORT_SYMBOL(sub_integar);

