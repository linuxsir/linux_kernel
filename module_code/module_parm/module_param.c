/*
 * =====================================================================================
 *
 *       Filename:  module_param.c
 *
 *    Description:  模块参数练习
 *
 *        Version:  1.0
 *        Created:  2015年09月10日 21时56分21秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/moduleparam.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("MAGUICAI");
MODULE_DESCRIPTION("module parameters test");
#define MAX_ARRAY   6
static int int_var = 0;
static char *str_var = "default";
static int int_array[6];
int narr;

module_param(int_var,int,0644);
MODULE_PARM_DESC(int_var,"A integer variable");
module_param(str_var,charp,0644);
MODULE_PARM_DESC(str_var,"A string Variable");
module_param_array(int_array,int,&narr,0644);
MODULE_PARM_DESC(int_array,"a integer array");
static int __init myparam_init(void)
{
    int i ;
    printk(KERN_ALERT "hello,my dear\n");
    printk(KERN_ALERT "int_var = %d\n",int_var );
    printk(KERN_ALERT "str_var = %s\n",str_var );
    for (i = 0; i < narr; ++i) {
        printk(KERN_ALERT "int_array[%d]=%d\n",i,int_array[i] );
    }
    return 0;
}
static void __exit myparam_exit(void)
{
    printk(KERN_ALERT "Bye,my dear!\n");

}
module_init(myparam_init);
module_exit(myparam_exit);


